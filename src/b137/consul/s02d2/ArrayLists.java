package b137.consul.s02d2;

import java.util.ArrayList;

public class ArrayLists {

    public static void main(String[] args){
        System.out.println("ArrayLists\n");

        // Syntax
        // ArrayList<data_type> <identifier> = new ArrayList<data_type>();

        ArrayList<String> theBeatles = new ArrayList<String>(); // declaring a new array list

        //Inserting a new item(s) and retrieving existing item(s) in an array list
        theBeatles.add("John");
        System.out.println(theBeatles.get(0));
        theBeatles.add("Paul");
        System.out.println(theBeatles);

        // Updating item(s) of an array list
        theBeatles.set(0, "John Lennon");
        System.out.println((theBeatles.get(0)));

        //Remove existing item(s) in an array list
        theBeatles.add("Steph Curry");
        System.out.println(theBeatles);
        theBeatles.remove(2);
        System.out.println(theBeatles);

        // Remove all items in an array list
        theBeatles.clear();
        System.out.println(theBeatles);
    }
}
